﻿open System.Diagnostics
open System.IO
open TwitchLib.Client.Models
open TwitchLib.Communication.Models
open System
open TwitchLib.Communication.Clients
open TwitchLib.Client
open System.Text.RegularExpressions

let fileName = "secrets.txt"

[<EntryPoint>]
let main argv =
    let lines = File.ReadAllLines(File.ReadAllText(fileName))

    let userName = lines.[0]
    let authToken = lines.[1]
    let channelName = lines.[2]
    let pattern = lines.[3]
    let processFileName = lines.[4]
    let processArguments = lines.[5]
    let processDirectory = lines.[6]

    use clientProcess = new Process()
    clientProcess.StartInfo.FileName <- processFileName
    clientProcess.StartInfo.Arguments <- processArguments
    clientProcess.StartInfo.WorkingDirectory <- processDirectory
    clientProcess.StartInfo.UseShellExecute <- false
    clientProcess.StartInfo.RedirectStandardInput <- true
    clientProcess.Start() |> ignore

    let credentials = ConnectionCredentials(userName, authToken)
    let clientOptions = ClientOptions(MessagesAllowedInPeriod=750, ThrottlingPeriod=(TimeSpan.FromSeconds(30.0)))

    let customClient = WebSocketClient(clientOptions)
    let client = TwitchClient(customClient)
    client.Initialize(credentials, channelName)
    client.OnLog.Add
        (fun e -> 
            if e.Data.Contains(pattern) then
                let m = Regex.Match(e.Data, @"PRIVMSG\s*#[^\s]*\s*:(.*)")
                let message = m.Groups.[1].Value
                System.Console.WriteLine(message)
                clientProcess.StandardInput.WriteLine(message))
    client.Connect()

    while not clientProcess.HasExited do ()

    clientProcess.Close()
    0
